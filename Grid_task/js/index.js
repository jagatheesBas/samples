var express = new require('express');
var nunjucks = require( 'nunjucks' ) ;
var app = express() ;
var path = require("path");
var fs = require('fs');

var jsonURL = "data/data.json";

var PATH_TO_TEMPLATES = '.' ;
nunjucks.configure( PATH_TO_TEMPLATES, {
    autoescape: true,
    express: app
} ) ;

app.get( '/', function( req, res ) {
  app.use(express.static(__dirname + '/../'));

  fs.readFile(jsonURL, function (err, data) {
    if(!err) {
      data = JSON.parse(data);
      res.render('index.html', data);
    }
    else {
      res.render('pageNotFound.html');
      console.log(err)
    }
  });
} ) ;
  
app.listen(8000, function(){
    console.log("Listening on port:"+"http://localhost:8000");
});
